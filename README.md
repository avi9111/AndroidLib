#AndroidLib
Android项目专用，比较完善可debug的is library库
##引入步骤
1. 右键项目，选择Properties，打开Properties for [project name]界面
2. 选择左边Android选项
3. Add选择此项目（一般必须和原项目在同一个根目录，否则会出事）

##版本
20151210 v1.0
1.0版本只算是比较完善而已，可直接用于DEBUG新组件代码，但个别问题还未解决。
下列情况：请不要使用本lib，
*项目已经引用友盟的sdk，因为友盟的sdk和这个lib其实是同一个方法生成的项目，所以R或者layout的一些命名有冲突
